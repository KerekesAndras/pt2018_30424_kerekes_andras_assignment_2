package lidlQueue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import entity.Client;

//extends the Thread Class in order to use its methods
public class LIDLCashier extends Thread {
	
	/**
	 * finalMessage to print in GUI
	 * runFlag for the while(true) loop 
	 * threadSleepingTime to make the threads wait
	 * each LidlCashier has a cashierMNumber,totalServiceTime,mOfClients and a BlockingQueue
	 * **/
	public String finalMessage;
	
	private float finalAverrageTime=0;
	private int queueFlag=-1;
	private final int runFlag=0;
	private double threadSleepingTime=60;
	
	private int cashierNumber ;
	private int totalServiceTime=0;
	private int nrOfClients;
	private BlockingQueue<Client> clientQueue;

	@Override
	public void run() {
		
		while(runFlag==0){
			
				try{
		/**
		 *first we create a new client
		 *next we take the client from the queue
		 * make the thread sleep until it processes the client
		 * we add the client process time to the total service time and the number of clients in this queue grows with 1 
		 * **/
				String outputResult=null;
				Client client=null;
				client=takeOneClientFromQueue();   
				
				System.out.println("Cashier nr:" +cashierNumber+"--->"+"Client "+client.getName()+" is being served");
				
				Thread.sleep((long) (2*threadSleepingTime*client.getProcessingTime())); 
				
				totalServiceTime=totalServiceTime+client.getProcessingTime(); 
				
				nrOfClients=nrOfClients+1;
				
				outputResult="Client "+client.getId()+"("+ client.getName()+")"+"was served.Have a good day!";
				
				System.out.println(outputResult);	
				
				}
				catch(InterruptedException exception){
					System.out.println("ERROR");
					System.out.println("Can't process the client queue");
				}
				finally{
					//at the end of simulation to show the average time of each queue
					setFinalAverrageTime(getAverageServiceTime());
				}
			}
	}
	
	public LIDLCashier(int i){   
		this.cashierNumber=i;
		clientQueue=new LinkedBlockingQueue<Client>();
	}
	
	public LIDLCashier(){ 
		super();
	}
	
	public int isBlockingQueueEmpty(){
	
			if(clientQueue.isEmpty()==true) {
				this.queueFlag=1;
			}
			else {
				this.queueFlag=0;
			}
	
		return queueFlag;
	}
	/**
	 * each LidlCashier will have its own average service time
	 * 1 was added in case of cashier has no clients(divide by zero)
	 * **/
	public float getAverageServiceTime(){
		return (float) ((1+totalServiceTime)/(1+nrOfClients));
	}
	
	/**
	 * method to add a client to a queue
	 * 
	 * **/
	public void addClientToQueue(Client client){
		
		clientQueue.add(client);
		String addClientMessage=null;
		addClientMessage="Client:"+client.getName()+" has been added to the Queue:"+cashierNumber+" having processing time:"+client.getProcessingTime()+" at the moment:"+client.getArrivalTime();
		System.out.println(addClientMessage);
		this.finalMessage=this.finalMessage+addClientMessage+"\n";
	}

	public int getNrOfClientsInQueue(){
		int size=clientQueue.size();
		return size;
	}
	
	public boolean removeClientFromQueue() {
		return clientQueue.remove() != null;
	}
	
	public int existClientInQueue(Client c) {
		if(clientQueue.contains(c)==false) {
			return 0;
		}
		else {
			return 1;
		}
	}
	
	public Client takeOneClientFromQueue() throws InterruptedException {
		Client client=clientQueue.take();
		return client;
	}

	public float getFinalAverrageTime() {
		return finalAverrageTime;
	}

	public void setFinalAverrageTime(float finalAverrageTime) {
		this.finalAverrageTime = finalAverrageTime;
	}

	public int getQueueFlag() {
		return queueFlag;
	}

	public void setQueueFlag(int queueFlag) {
		this.queueFlag = queueFlag;
	}

	public int getRunFlag() {
		return runFlag;
	}

	

}
