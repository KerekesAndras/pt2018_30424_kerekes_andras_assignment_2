package lidlQueue;

import entity.Client;
import lidlQueue.LIDLCashier;

public class LIDLQueue {

	/**
	 * each queue has its own thread and it is assigned a cashier
	 * LidlQueue1 is assigned to LidlCashier1
	 * **/
	private final int averageTimeaux = 3;//used when computing the average time in getting a better float result
	Thread thread;
	private float totalTime;
	private int nrOfClients;
	private LIDLCashier cashier;

	public LIDLQueue(int x) {
		cashier = new LIDLCashier(x);
		thread = new Thread(cashier);
		thread.start();
	}

	public LIDLCashier getCashier() {
		return cashier;
	}


	public String getCashierDetails() {
		String details = null;
		details = "Cashier details: " + cashier.getName() + " " + cashier.getState();
		return details;
	}

	public boolean addClient(Client client) {
		nrOfClients++;
		totalTime += client.getProcessingTime();
		cashier.addClientToQueue(client);
		return true;
	}
	
	public void getThreadStatus() {
		thread.getStackTrace();	
		}

	public float getAverageTotalTime() {
		return (float) (averageTimeaux * (totalTime / nrOfClients) / averageTimeaux);
	}

	public int getAverageTimeaux() {
		return averageTimeaux;
	}

}
