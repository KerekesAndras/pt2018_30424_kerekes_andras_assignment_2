package gui;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.Timer;

import application.LIDL;
import entiry_generator.GenerateClients;
import lidlQueue.LIDLCashier;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ApplicationView {

	private JFrame frame;
	private JTextField textField_minProc;
	private JTextField textField_maxProc;
	private JTextField textField_Total;
	JTextPane textPane ;
	JTextPane textData = new JTextPane();
	GenerateClients clients=new GenerateClients();		
	Thread thread=new Thread(clients);


	/**
	 * this is an ActionPerformer by the timer with a delay of 100 milliseconds
	 * to print in the textPane of GUI
	 * **/
	public ApplicationView() {			
		 initialize();
		 int delay = 100; //milliseconds
	      ActionListener taskPerformer = new ActionListener() {
	          public void actionPerformed(ActionEvent evt) {
	              String date = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date(System.currentTimeMillis()));
	              textPane.setText(clients.getQueues()[1].getCashier().finalMessage);
	              textPane.setText(clients.getQueues()[2].getCashier().finalMessage);
	              textData.setText(date);
	              //textPane.setText(clients.getQueues()[3].getCashier().finalMessage);
	              //textPane.repaint();
	          }
	      };
	     new Timer(delay, taskPerformer).start();
	     //System.err.println(c.finalMessage);
		 frame.setVisible(true);		
		
	}
	
	

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(153, 255, 153));
		frame.setBounds(100, 100,700, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		
		textData.setBounds(10, 11, 60, 20);
		frame.getContentPane().add(textData);
		
		textPane = new JTextPane();
		textPane.setBounds(350, 36, 279, 500);
		frame.getContentPane().add(textPane);
		
		textField_minProc = new JTextField();
		textField_minProc.setBounds(88, 67, 108, 20);
		frame.getContentPane().add(textField_minProc);
		textField_minProc.setColumns(10);
		
		textField_maxProc = new JTextField();
		textField_maxProc.setBounds(88, 120, 108, 20);
		frame.getContentPane().add(textField_maxProc);
		textField_maxProc.setColumns(10);
		
		JLabel lblMin = new JLabel("Min");
		lblMin.setBounds(10, 70, 68, 14);
		frame.getContentPane().add(lblMin);
		
		JLabel lblMax = new JLabel("Max");
		lblMax.setBounds(10, 123, 68, 14);
		frame.getContentPane().add(lblMax);
		
		JButton btnStartSimulation = new JButton("Start Simulation");
		btnStartSimulation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(textField_Total.getText().isEmpty() || textField_maxProc.getText().isEmpty()|| textField_minProc.getText().isEmpty()){
					JOptionPane.showMessageDialog(null, "Please introduce all parameters");
				}
				else{
					/**
					 * take the input parameters from the GUI and start the thread 
					 * "parseInt" to transform from string into Int
					 * **/
				 clients.setApplicationRunTime(Integer.parseInt(textField_Total.getText()));
				 clients.setMaxProcessingTime(Integer.parseInt(textField_maxProc.getText()));
				 clients.setMinProcessingTime(Integer.parseInt(textField_minProc.getText()));	
				 thread.start();
				}
				
			}
		});
		btnStartSimulation.setForeground(Color.DARK_GRAY);
		btnStartSimulation.setBackground(new Color(0, 204, 255));
		btnStartSimulation.setBounds(64, 210, 132, 23);
		frame.getContentPane().add(btnStartSimulation);
		
		JLabel lblSimulationResults = new JLabel("SIMULATION RESULTS");
		lblSimulationResults.setBounds(433, 11, 132, 14);
		frame.getContentPane().add(lblSimulationResults);
		
		textField_Total = new JTextField();
		textField_Total.setBounds(88, 160, 108, 20);
		frame.getContentPane().add(textField_Total);
		textField_Total.setColumns(10);
		
		JLabel lblTotal = new JLabel("Total");
		lblTotal.setBounds(10, 163, 46, 14);
		frame.getContentPane().add(lblTotal);
	}
}
