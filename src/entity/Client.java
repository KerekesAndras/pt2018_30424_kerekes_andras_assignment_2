package entity;

public class Client {
		
	/**
	 * the Client class with its atttributes
	 * **/
	 private int id;
	 private String name;
	 private int arrivalTime ;
	 private int processingTime;
	 	 	 
	public Client(int id, String name, int arrivalTime, int processingTime) {
		super();
		this.id = id;
		this.name = name;
		this.arrivalTime = arrivalTime;
		this.processingTime = processingTime;
	}

	public Client()
	{
		super();
	}
	
	public int getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public int getProcessingTime() {
		return processingTime;
	}
	public void setProcessingTime(int processingTime) {
		this.processingTime = processingTime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	 
	 
	 
	
}
