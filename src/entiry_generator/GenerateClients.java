package entiry_generator;

import java.util.Random;
import java.util.Scanner;

import entity.Client;
import lidlQueue.LIDLQueue;


public class GenerateClients implements Runnable {
	
	/**
	 * we have some random names in a string vector
	 * we have the vector of queues
	 * totalNrOfCashiers,applicationRunTime,maxProcessingTime,minProcessingTime---->all being given as inputs in GUI
	 * 
	 * **/
	 private String[] randomNames= new String[10];
	 private LIDLQueue[] queues;
	 private long threadSleepTime=50;//amount of thread sleep added to ensure synchronization 
	 private int totalNrOfCashiers;
	 private int applicationRunTime;
	 private int maxProcessingTime;
	 private int minProcessingTime;
	 
	 
	/**
	 * Here we generate 3 queues
	 */
	 private void populateStringArray()
	 {
		 this.randomNames[0] = "Ion";
		 this.randomNames[1] = "Mircea";
		 this.randomNames[2] = "Oliver";
		 this.randomNames[3] = "Mihaela";
		 this.randomNames[4] = "Andras";
		 this.randomNames[5] = "Jake";
		 this.randomNames[6] = "Emil";
		 this.randomNames[7] = "David";
		 this.randomNames[8] = "Cristian";
		 this.randomNames[9] = "Claudia";

	 }
	 
	 public String[] getRandomNames() {
		return randomNames;
	}

	public void setRandomNames(String[] randomNames) {
		this.randomNames = randomNames;
	}

	public LIDLQueue[] getQueues() {
		return queues;
	}

	public void setQueues(LIDLQueue[] queues) {
		this.queues = queues;
	}

	public long getThreadSleepTime() {
		return threadSleepTime;
	}

	public void setThreadSleepTime(long threadSleepTime) {
		this.threadSleepTime = threadSleepTime;
	}

	public int getTotalNrOfCashiers() {
		return totalNrOfCashiers;
	}

	public void setTotalNrOfCashiers(int totalNrOfCashiers) {
		this.totalNrOfCashiers = totalNrOfCashiers;
	}

	//the constructor will populate the array and create 3 queues and 3 cashiers
	public GenerateClients(){
		 populateStringArray();
		 totalNrOfCashiers=4;
		 queues =new LIDLQueue[totalNrOfCashiers];  //VECTOR DE QUEUE-URI
		 for(int i=0;i<=totalNrOfCashiers-1;i++){
			 queues[i]=new LIDLQueue(i);
		 }
	 }
	 	 
	//
	 public int selectOptimalQueue() {
		 
			int aux = 0;		
			for (int i = 0; i < totalNrOfCashiers - 1; i++) {
				if (queues[totalNrOfCashiers - i - 1].getCashier().isBlockingQueueEmpty()==1) {
					return totalNrOfCashiers - i - 1;
					//first insert in queue 2 then if in queue 1 there are more people than in queue 3 it will put it in queue 3
				} else if (queues[i].getCashier().getNrOfClientsInQueue() > queues[i + 1].getCashier().getNrOfClientsInQueue()) {
					aux = i + 1;
				}
			}
			return aux;
		}
	 
	 public void run(){
		 int currentTime=0;
		 int choseQueue;
		 Random randomGenerator = new Random();
		 
		 while(currentTime<applicationRunTime){
			 /**
			  * First we choose a queue,then we create a client with a minimum processing time,a random name ,an arrival time 
			  * and we add him to the choosen/selected queue.
			  * 
			  * **/
			 choseQueue=selectOptimalQueue();
		     int index = randomGenerator.nextInt(9);
			 int processTime =  minProcessingTime + (int) (Math.random()*(maxProcessingTime - minProcessingTime));
			 Client currentClient=new Client((1/2*(currentTime+currentTime)*(currentTime+currentTime+1))+currentTime,randomNames[index],currentTime,processTime);
			 queues[choseQueue].addClient(currentClient);
			 
			 try{
				 Thread.sleep(threadSleepTime*2*2);
			 }
			 catch(InterruptedException o){
				 System.out.println("ERROR.Something went wrong");
			 }
			 //after inserting a client the current time grows
			 currentTime=currentTime+1;
			 currentTime++;			 
		 }
		 
		 /**
		  * Here is the final output for all cashiers and queues
		  * **/
		 for(int j=0;j<totalNrOfCashiers;j++){
			 System.err.println("Average waiting time in Queue"+j+" is "+queues[j].getAverageTotalTime());
			 System.err.println("Average processing time at Cashier" +j+" is "+queues[j].getCashier().getAverageServiceTime());
		 }
		 System.err.println("FINISH");
		// System.exit(0); 
	 }
	/**
	 * @return the applicationRunTime
	 */
	public int getApplicationRunTime() {
		return applicationRunTime;
	}
	/**
	 * @param applicationRunTime the applicationRunTime to set
	 */
	public void setApplicationRunTime(int applicationRunTime) {
		this.applicationRunTime = applicationRunTime;
	}
	/**
	 * @return the maxProcessingTime
	 */
	public int getMaxProcessingTime() {
		return maxProcessingTime;
	}
	/**
	 * @param maxProcessingTime the maxProcessingTime to set
	 */
	public void setMaxProcessingTime(int maxProcessingTime) {
		this.maxProcessingTime = maxProcessingTime;
	}
	/**
	 * @return the minProcessingTime
	 */
	public int getMinProcessingTime() {
		return minProcessingTime;
	}
	/**
	 * @param minProcessingTime the minProcessingTime to set
	 */
	public void setMinProcessingTime(int minProcessingTime) {
		this.minProcessingTime = minProcessingTime;
	}
	 
	 

}
